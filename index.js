import dotenv from 'dotenv';
dotenv.config();

import express from 'express';

const app = express();

app.get('/', function(req, res) {
    res.json({
        message: 'root welcome',
        ok: true,
        status: 200,
    });
});

app.get('/games', function(req, res) {
    res.json([
        {
            name: 'suit',
        },
        {
            name: 'werewolf',
        },
    ]);
});

app.get('/users', function(req, res) {
    res.json([
        {
            name: 'ayu',
        },
        {
            name: 'dewi',
        },
        {
            name: 'bayu',
        },
    ]);
})

app.listen(process.env.PORT, function() {
    console.info(`server listen on port: ${process.env.PORT}`)
});
